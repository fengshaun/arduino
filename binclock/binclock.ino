const int dataPin = 8;
const int clockPin = 11;
const int latchPin = 12;

const int secPin = 7;
bool secPinOn = false;

const int setPin = 4;
const int hrSetPin = 3;
const int minSetPin = 2;

int sec = 0;
int min = 1;
int hr = 0;

void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(secPin, OUTPUT);

  pinMode(setPin, INPUT);
  pinMode(hrSetPin, INPUT);
  pinMode(minSetPin, INPUT);
}

void loop() {
  if (digitalRead(setPin)) {
    digitalWrite(secPin, LOW);
    sec = 0;

    if (digitalRead(hrSetPin)) {
      hr++;
      delay(250);
    }

    if (digitalRead(minSetPin)) {
      min++;
      delay(250);
    }

    if (hr >= 24) {
      hr = 0;
    }

    if (min >= 60) {
      min = 0;
    }

    showClock();

  } else {
    if (secPinOn) {
      secPinOn = false;
      digitalWrite(secPin, LOW);
    } else {
      secPinOn = true;
      digitalWrite(secPin, HIGH);
    }

    sec++;

    if (sec >= 60) {
      sec = 0;
      min++;
    }

    if (min >= 60) {
      hr++;
      min = 0;
    }

    if (hr >= 24) {
      hr = 0;
    }

    showClock();
    delay(1000);
  }
}

void showClock() {
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, hr);
  shiftOut(dataPin, clockPin, MSBFIRST, min);
  digitalWrite(latchPin, HIGH);
}
