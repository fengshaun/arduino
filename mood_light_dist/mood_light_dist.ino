#include "led.h"

#define IR_IN_PIN A0
#define BUTTON_PIN 2

#define LEDS_LENGTH 3
LED leds[] = {
    {3, 0, "green"},
    {5, 0, "yellow"},
    {6, 0, "red"},
};

int8_t selected_led = 0;

void next_led()
{
    if (selected_led == LEDS_LENGTH - 1) {
        selected_led = -1;
    } else {
        selected_led++;
    }
}

int read_ir_sensor()
{
    // should sanitize/filter the data
    return analogRead(IR_IN_PIN);
}

void setup() {
    for (int i = 0; i < LEDS_LENGTH; i++) {
        pinMode(leds[i].pin, OUTPUT);
    }

    pinMode(IR_IN_PIN, INPUT);
    pinMode(BUTTON_PIN, INPUT);
    Serial.begin(9600);
}

void loop() {
    if (digitalRead(BUTTON_PIN) == HIGH) {
        next_led();
        delay(500);
    } else {
        if (selected_led >= 0 && selected_led < LEDS_LENGTH) {
            led_set_brightness(&leds[selected_led], map(read_ir_sensor(), 0, 600, 255, 0));
        }
    }
    delay(10);
}
