#include "led.h"

const int led_length = 6;
LED leds[] = {
    {0, 3, "green"},
    {0, 5, "yellow"},
    {0, 6, "red"},
    {0, 9, "white"},
    {0, 10, "yellow"},
    {0, 11, "green"},
};

int current_led = 0;
int step = 20;
bool direction = true; //true -> forward, false -> backward

void setup() {
    for (int i = 0; i < led_length; i++) {
        pinMode(leds[i].pin, OUTPUT);
    }

    Serial.begin(9600);
}

void loop() {
    LED *led = &leds[current_led];
    int new_brightness = led_change_brightness(led, step);
    Serial.println(current_led);

    if (direction && new_brightness >= 255) {
        if (current_led >= 6) {
            step = -step;
            direction = !direction;
        } else {
            current_led++;
        }
    } else if (!direction && new_brightness <= 0) {
        if (current_led <= 0) {
            step = -step;
            direction = !direction;
        } else {
            current_led--;
        }
    }

    delay(30);
}

