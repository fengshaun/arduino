// 4 LED multiplex test
// to add more leds, just change the arrays

#define SWITCH_PIN 8
#define SRC_PINS_LENGTH 2
#define GND_PINS_LENGTH 2
#define STATE_LENGTH 3

uint8_t src_pins[] = {2, 3};
uint8_t gnd_pins[] = {5, 6};

uint8_t pin_states[STATE_LENGTH][SRC_PINS_LENGTH][GND_PINS_LENGTH] = {
    { {HIGH, HIGH},
      {LOW, HIGH},
    },
    { {LOW, LOW},
      {HIGH, HIGH},
    },
    { {HIGH, LOW},
      {LOW, HIGH},
    },
};

uint8_t current_state = 0;

//============== END OF CONFIGURATION ================//

void led_switch(uint8_t row, uint8_t col, uint8_t state) {
    // state is either HIGH or LOW
    digitalWrite(src_pins[row], state);
    digitalWrite(gnd_pins[col], state);
}

void switch_state() {
    if (current_state >= STATE_LENGTH - 1) {
        current_state = 0;
    } else {
        current_state++;
    }
}

void setup() {
    pinMode(SWITCH_PIN, INPUT);

    for (uint8_t i = 0; i < SRC_PINS_LENGTH; i++) {
        pinMode(src_pins[i], OUTPUT);
    }

    for (uint8_t i = 0; i< GND_PINS_LENGTH; i++) {
        pinMode(gnd_pins[i], OUTPUT);
    }

    Serial.begin(96000);
}

void loop() {
    for (uint8_t i = 0; i < SRC_PINS_LENGTH; i++) {
        for (uint8_t j = 0; j < GND_PINS_LENGTH; j++) {
            led_switch(i, j, pin_states[current_state][i][j]);
            delay(1);
            led_switch(i, j, LOW);
            delay(1);
        }
        delay(1);
    }
    delay(1);

    uint8_t s = digitalRead(SWITCH_PIN);
    if (s == HIGH) {
        switch_state();
        delay(500);
    }
    delay(1);
}
