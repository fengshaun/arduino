#include "notes.h"

#define PIEZO_PIN 9
void setup() {
    pinMode(PIEZO_PIN, OUTPUT);
}

int duration = 250; // ms
int notes[] = {NOTE_E4, NOTE_DS4, NOTE_E4, NOTE_DS4, NOTE_E4, NOTE_B3, NOTE_D4, NOTE_C4, NOTE_A3};
int NOTES_LENGTH = 9;

void loop() {
    // pin, freq, duration
    for (int i = 0; i < NOTES_LENGTH; i++) {
        tone(PIEZO_PIN, notes[i], duration);
        delay(duration * 1.1);
    }

    delay(1000);
}
