#include <Arduino.h>

typedef struct {
  char value;
  uint8_t state; // 0 for released, 1 for pressed
} key_t;

void press_key(key_t &key) {
  if (!key.state) {
    key.state = 1;
    Keyboard.press(key.value);
  }
}
  
void release_key(key_t &key) {
  if (key.state) {
    key.state = 0;
    Keyboard.release(key.value);
  }
}

