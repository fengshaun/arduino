#include "key.h"

uint8_t row_pins[] = {8, 9, 10, 11, 12};
const uint8_t row_count = 5;

uint8_t col_pins[] = {2, 3, 4, 5, 6, 7};
const uint8_t col_count = 6;

// the ordering depends on how the wires are laid out
uint8_t mod_pins[] = {A5, A4, A3};
const uint8_t mod_count = 3;
const uint8_t mod_read_pin = A0;

// the rows are reversed in hardware and I'm too lazy to fix it
key_t keys[row_count][col_count] = {
 {(key_t){';',0}, (key_t){'q',0}, (key_t){'j',0}, (key_t){'k',0}, (key_t){'x',0}, (key_t){'b',0}},
 {(key_t){'a',0}, (key_t){'o',0}, (key_t){'e',0}, (key_t){'u',0}, (key_t){'i',0}, (key_t){'d',0}},
 {(key_t){'\'',0}, (key_t){',',0}, (key_t){'.',0}, (key_t){'p',0}, (key_t){'y',0}, (key_t){'f',0}},
 {(key_t){'1',0}, (key_t){'2',0}, (key_t){'3',0}, (key_t){'4',0}, (key_t){'5',0}, (key_t){'6',0}},
 {(key_t){KEY_F1,0}, (key_t){KEY_F2,0}, (key_t){KEY_F3,0}, (key_t){KEY_F4,0}, (key_t){KEY_F5,0}, (key_t){KEY_F6,0}},
};

key_t mod_keys[mod_count] = {(key_t){KEY_LEFT_SHIFT,0}, (key_t){KEY_LEFT_CTRL,0}, (key_t){KEY_LEFT_ALT,0}};

void setup() {
  for (uint8_t i = 0; i < row_count; i++) {
    pinMode(row_pins[i], INPUT);
  }

  for (uint8_t i = 0; i < col_count; i++) {
    pinMode(col_pins[i], OUTPUT);
  }
  
  pinMode(mod_read_pin, INPUT);
  
  for (uint8_t i = 0; i < mod_count; i++) {
    pinMode(mod_pins[i], OUTPUT);
  }
  

  Serial.begin(9600);
  Keyboard.begin();
}

void loop() {
  // modifier keys
  for (uint8_t mod = 0; mod < mod_count; mod++) {
    digitalWrite(mod_pins[mod], HIGH);
    
    if (digitalRead(mod_read_pin) == HIGH) {
      press_key(mod_keys[mod]);
    } else {
      release_key(mod_keys[mod]);
    }
    
    digitalWrite(mod_pins[mod], LOW);
  }

  // normal keys  
  for (uint8_t col = 0; col < col_count; col++) {
    digitalWrite(col_pins[col], HIGH);

    for (uint8_t row = 0; row < row_count; row++) {
      if (digitalRead(row_pins[row]) == HIGH) { 
        press_key(keys[row][col]);
      } else {
        release_key(keys[row][col]);
      }
    }

    digitalWrite(col_pins[col], LOW);
  }
}


