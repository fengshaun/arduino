#ifndef LED_H
#define LED_H

#include <Arduino.h>

#define LED_MIN_BRIGHTNESS 0
#define LED_MAX_BRIGHTNESS 255

typedef struct {
    int pin;
    int brightness;
    char *color;
} LED;

int led_set_brightness(LED *led, int brightness) {
    if (brightness > LED_MAX_BRIGHTNESS) {
        brightness = LED_MAX_BRIGHTNESS;
    } else if (brightness < LED_MIN_BRIGHTNESS) {
        brightness = LED_MIN_BRIGHTNESS;
    }

    led->brightness = brightness;
    analogWrite(led->pin, led->brightness);
}

int led_change_brightness(LED *led, int step) {
    /* return type: final brightness */
    led->brightness += step;
    if (step < 0) {
        // negative step
        if (led->brightness < 0) {
            led->brightness = 0;
        }
    } else {
        // 0 or + step
        if (led->brightness > 255) {
            led->brightness = 255;
        }
    }

    // char txt[64];
    // sprintf(txt, "Writing: pin (%d) value(%d) color(%s)", led->pin, led->brightness, led->color);
    // Serial.println(txt);
    analogWrite(led->pin, led->brightness);

    return led->brightness;
}

void led_print_info(LED *led) {
    char tmp[32];
    Serial.println("---------------------");
    sprintf(tmp, "Brightness: %d", led->brightness);
    Serial.println(tmp);

    sprintf(tmp, "Pin: %d", led->pin);
    Serial.println(tmp);

    sprintf(tmp, "Color: %s", led->color);
    Serial.println(tmp);
    Serial.println("---------------------");
}

#endif

