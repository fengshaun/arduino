// sets the color of a light composed of multiple
// LEDs depending on the environment light intensity

#include "led.h"

#define LIGHT_IN_PIN A0
#define LED_LENGTH 4
LED leds[] = {
    // {pin, brightness, color}
    {3, 0, "green"},
    {5, 0, "red"},
    {6, 0, "green"},
    {9, 0, "red"},
};

void update_color() {
    // photocell's range is from 0 to 350
    unsigned int light_level = analogRead(LIGHT_IN_PIN);

    led_set_brightness(&leds[0], map(light_level, 0, 350, 255, 0)); 
    led_set_brightness(&leds[2], map(light_level, 0, 350, 255, 0)); 
    led_set_brightness(&leds[1], map(light_level, 0, 350, 0, 255)); 
    led_set_brightness(&leds[3], map(light_level, 0, 350, 0, 255)); 
}

void setup() {
    Serial.begin(9600);

    for (uint8_t i = 0; i < LED_LENGTH; i++) {
        pinMode(leds[i].pin, OUTPUT);
    }

    pinMode(LIGHT_IN_PIN, INPUT);
}

void loop() {
    update_color();
    Serial.println(analogRead(LIGHT_IN_PIN));
    delay(10); // every 1 second for testing purposes
}

